@extends('layouts.portfolio')

@section('content')
    <!-- ======= Works Section ======= -->
    <section class="section site-portfolio">
        <div class="container">
            <div class="row mb-5 align-items-center">
                <div class="col-md-12 col-lg-6 mb-4 mb-lg-0" data-aos="fade-up">
                    <h2>Hey, I'm Aqris T. Satria</h2>
                    <p class="mb-0">an Creative &amp; Professional Web Developer</p>
                </div>
                <div class="col-md-12 col-lg-6 text-start text-lg-end" data-aos="fade-up" data-aos-delay="100">
                    <div id="filters" class="filters">
                        <a href="#" data-filter="*" class="active">All</a>
                        <a href="#" data-filter=".web">Web</a>
                        <a href="#" data-filter=".mobile">Mobile</a>
                        <a href="#" data-filter=".kiosk">Kiosk</a>
                    </div>
                </div>
            </div>
            <div id="portfolio-grid" class="row no-gutter" data-aos="fade-up" data-aos-delay="200">
                @forelse ($projects as $project)
                    <div class="item {{ $project->category }} col-sm-6 col-md-4 col-lg-4 mb-4">
                        <a href="{{ route('project.byslug', $project->slug) }}" class="item-wrap fancybox">
                            <div class="work-info">
                                <h3>{{ $project->name }}</h3>
                                <span>{{ $project->category }}</span>
                            </div>
                            <img class="img-fluid" src="{{ Storage::url($project->portfolio) }}">
                        </a>
                    </div>
                @empty
                    No Data
                @endforelse


            </div>
        </div>
    </section><!-- End  Works Section -->

    <!-- ======= Clients Section ======= -->
    <section class="section">
        <div class="container">
            <div class="row justify-content-center text-center mb-4">
                <div class="col-8">
                    <h3 class="h3 heading">My Clients</h3>
                    <p>Discover the stories behind my meaningful collaborations.</p>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-4 col-sm-4 col-md-2">
                    <a href="#" class="client-logo"><img src="assets/logo/logo_bp.png" alt="Image"
                            class="img-fluid"></a>
                </div>
                <div class="col-4 col-sm-4 col-md-2">
                    <a href="#" class="client-logo"><img src="assets/logo/logo_shell.png" alt="Image"
                            class="img-fluid"></a>
                </div>
                <div class="col-4 col-sm-4 col-md-2">
                    <a href="#" class="client-logo"><img src="assets/logo/logo_helix.png" alt="Image"
                            class="img-fluid"></a>
                </div>
                <div class="col-4 col-sm-4 col-md-2">
                    <a href="#" class="client-logo"><img src="assets/logo/logo_grab.png" alt="Image"
                            class="img-fluid"></a>
                </div>
                <div class="col-4 col-sm-4 col-md-2">
                    <a href="#" class="client-logo"><img src="assets/logo/logo_ipba.png" alt="Image"
                            class="img-fluid"></a>
                </div>
            </div>
        </div>
    </section><!-- End Clients Section -->

    <!-- ======= Services Section ======= -->
    <section class="section services">
        <div class="container">
            <div class="row justify-content-center text-center mb-4">
                <div class="col-8">
                    <h3 class="h3 heading">My Services</h3>
                    <p>I am a passionate and versatile developer with expertise in both web and mobile app development.
                        Here's a breakdown of my skills and a brief description of each:</p>
                </div>
            </div>
            <div class="row justify-content-center">

                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <i class="bi bi-card-checklist"></i>
                    <h4 class="h4 mb-2">Web Development</h4>
                    <ul class="list-unstyled list-line">
                        <li>HTML5/CSS3</li>
                        <li>JavaScript (ES6+)</li>
                        <li>PHP/Laravel Framework</li>
                        <li>React.js</li>
                        <li>Database (MongoDB, MySQL, PostgreSQL)</li>
                        <li>RESTful API Design</li>
                        <li>Version Control (Git)</li>
                    </ul>
                </div>
                <div class="col-12 col-sm-6 col-md-6 col-lg-3">
                    <i class="bi bi-binoculars"></i>
                    <h4 class="h4 mb-2">Mobile Applications</h4>
                    <ul class="list-unstyled list-line">
                        <li>Flutter</li>
                        <li>Dart</li>
                        <li>iOS Development (Swift)</li>
                        <li>Android Development (Java/Kotlin)</li>
                        <li>Mobile UI/UX Design</li>
                        <li>Firebase Integration</li>
                    </ul>
                </div>
            </div>
        </div>
    </section><!-- End Services Section -->
@endsection
