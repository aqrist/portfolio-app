@extends('layouts.portfolio')

@section('title')
    About
@endsection
@section('content')
    <section class="section pb-5">
        <div class="container">
            <div class="row mb-5 align-items-end">
                <div class="col-md-6" data-aos="fade-up">

                    <h2>About Me</h2>
                    <p class="mb-0">Hello, I'm Aqris T. Satria, a dedicated web and mobile developer with a passion for
                        creating innovative digital solutions. With a background deeply rooted in technology, I thrive on
                        transforming ideas into functional and aesthetically pleasing applications that leave a lasting
                        impact.</p>
                </div>

            </div>

            <div class="row">
                <div class="col-md-4 ml-auto order-2" data-aos="fade-up">
                    <h3 class="h3 mb-4">Skills</h3>
                    <ul class="list-unstyled">
                        <li class="mb-3">
                            <div class="d-flex mb-1">
                                <strong>PHP/Laravel</strong>
                                <span class="ml-auto">96%</span>
                            </div>
                            <div class="progress custom-progress">
                                <div class="progress-bar" role="progressbar" style="width: 96%" aria-valuenow="96"
                                    aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li class="mb-3">
                            <div class="d-flex mb-1">
                                <strong>UI/UX</strong>
                                <span class="ml-auto">78%</span>
                            </div>
                            <div class="progress custom-progress">
                                <div class="progress-bar" role="progressbar" style="width: 78%" aria-valuenow="78"
                                    aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li class="mb-3">
                            <div class="d-flex mb-1">
                                <strong>HTML5/CSS3</strong>
                                <span class="ml-auto">99%</span>
                            </div>
                            <div class="progress custom-progress">
                                <div class="progress-bar" role="progressbar" style="width: 99%" aria-valuenow="99"
                                    aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                        <li class="mb-3">
                            <div class="d-flex mb-1">
                                <strong>Dart/Flutter</strong>
                                <span class="ml-auto">87%</span>
                            </div>
                            <div class="progress custom-progress">
                                <div class="progress-bar" role="progressbar" style="width: 87%" aria-valuenow="87"
                                    aria-valuemin="0" aria-valuemax="100"></div>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="col-md-7 mb-5 mb-md-0" data-aos="fade-up">
                    {{-- <p><img src="assets/img/person_1_sq.jpg" alt="Image" class="img-fluid"></p> --}}
                    <p>
                        I hold a strong foundation in web and mobile development, specializing in a variety of technologies
                        to bring projects to life. My journey in the tech world has equipped me with the skills needed to
                        craft responsive and efficient web interfaces, as well as seamless mobile applications.
                    </p>
                </div>

            </div>

        </div>

    </section>
@endsection
