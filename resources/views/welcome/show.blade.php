@extends('layouts.portfolio')


@section('title')
    {{ $project->name }}
@endsection

@section('content')
    <section class="section">

        <div class="site-section pb-0">
            <div class="container">
                <div class="row align-items-stretch">
                    <div class="col-md-8" data-aos="fade-up">
                        <img src="{{ Storage::url($project->portfolio) }}" alt="Image" class="img-fluid">
                    </div>
                    <div class="col-md-3 ml-auto" data-aos="fade-up" data-aos-delay="100">
                        <div class="sticky-content">
                            <h3 class="h3">{{ $project->name }}</h3>
                            <p class="mb-4"><span class="text-muted">{{ $project->web }}</span></p>

                            <div class="mb-5">
                                <p>
                                    {!! $project->description !!}
                                </p>

                            </div>

                            {{-- <h4 class="h4 mb-3">What I did</h4>
                            <ul class="list-unstyled list-line mb-5">
                                <li>Design</li>
                                <li>HTML5/CSS3</li>
                                <li>CMS</li>
                                <li>Logo</li>
                            </ul> --}}

                            @if ($project->url != null)
                                <p><a href="{{ $project->url }}" class="readmore">Visit Website</a></p>
                            @else
                            @endif

                        </div>
                    </div>
                </div>
            </div>
    </section>
@endsection
