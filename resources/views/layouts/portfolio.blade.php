<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>
        Aqris T. Satria
        @yield('title')
    </title>

    <meta
        content="Hello, I'm Aqris T. Satria, a dedicated web and mobile developer with a passion for
    creating innovative digital solutions. With a background deeply rooted in technology, I thrive on
    transforming ideas into functional and aesthetically pleasing applications that leave a lasting
    impact."
        name="description">
        
    <meta content="web, html, css, js, laravel, mobile, app, development" name="keywords">

    @include('includes.css')
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-25YYY0BM44"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'G-25YYY0BM44');
    </script>
</head>

<body>

    <!-- ======= Navbar ======= -->
    @include('includes.navbar')

    <main id="main">

        @yield('content')


    </main><!-- End #main -->

    <!-- ======= Footer ======= -->
    @include('includes.footer')

    @include('includes.js')

</body>

</html>
