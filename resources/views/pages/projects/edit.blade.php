@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <h2>Edit Project</h2>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form action="{{ route('projects.update', $project->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            @method('PUT') {{-- Use PUT method for updates --}}

            {{-- Existing project details --}}
            <input type="hidden" name="id" value="{{ $project->id }}">

            <div class="mb-3">
                <label for="name" class="form-label">Name</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $project->name }}"
                    required>
            </div>
            <div class="mb-3">
                <label for="category" class="form-label">Category</label>
                <input type="text" class="form-control" id="category" name="category" value="{{ $project->category }}"
                    required>
            </div>
            <div class="mb-3">
                <label for="portfolio" class="form-label">Portfolio Image</label>
                <input type="file" class="form-control" id="portfolio" name="portfolio">
            </div>
            <div class="mb-3">
                <label for="url" class="form-label">Url *if any</label>
                <input type="text" class="form-control" id="url" name="url" value="{{ $project->url }}">
            </div>

            {{-- CKEditor container --}}
            <div class="mb-3">
                <label for="description" class="form-label">Description (CKEditor)</label>
                <textarea class="form-control" id="description" name="description" rows="10">{{ $project->description }}</textarea>
            </div>

            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
@endsection

@push('addon-script')
    {{-- CKEditor CDN --}}
    <script src="https://cdn.ckeditor.com/ckeditor5/23.0.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor.create(document.querySelector('#description'))
            .catch(error => {
                console.error(error);
            });
    </script>
@endpush
