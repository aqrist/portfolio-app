@extends('layouts.app')

@section('content')
    <div class="container mt-5">
        <h2>Project Details</h2>
        <a href="{{ route('projects.index') }}" class="btn btn-secondary mb-3">Back to List</a>

        <dl class="row">
            <dt class="col-sm-3">ID</dt>
            <dd class="col-sm-9">{{ $item->id }}</dd>

            <dt class="col-sm-3">Name</dt>
            <dd class="col-sm-9">{{ $item->name }}</dd>

            <dt class="col-sm-3">Category</dt>
            <dd class="col-sm-9">{{ $item->category }}</dd>

            <dt class="col-sm-3">Description</dt>
            <dd class="col-sm-9">{!! $item->description !!}</dd>

            <dt class="col-sm-3">Portfolio Image</dt>
            <dd class="col-sm-9">
                @if ($item->portfolio)
                    <img src="{{ Storage::url($item->portfolio) }}" alt="Portfolio Image" class="img-fluid">
                @else
                    No image available.
                @endif
            </dd>
        </dl>
    </div>
@endsection
