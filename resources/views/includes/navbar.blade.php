 <!-- ======= Navbar ======= -->
 <div class="collapse navbar-collapse custom-navmenu" id="main-navbar">
     <div class="container py-2 py-md-5">
         <div class="row align-items-start">
             <div class="col-md-2">
                 <ul class="custom-menu">
                     <li class="active"><a href="{{ route('/') }}">Home</a></li>
                     <li><a href="{{ route('about') }}">About Me</a></li>
                 </ul>
             </div>
             <div class="col-md-6 d-none d-md-block  mr-auto">
                 <div class="tweet d-flex">

                 </div>
             </div>
             <div class="col-md-4 d-none d-md-block">
                 <h3>Hire Me</h3>
                 <p>Thank you for visiting my profile, and I look forward to the opportunity to bring your digital ideas
                     to life!<br> <a href="#">aqrist@gmail.com</a>
                 </p>
             </div>
         </div>

     </div>
 </div>

 <nav class="navbar navbar-light custom-navbar">
     <div class="container">
         <a class="navbar-brand" href="{{ route('/') }}">AqrisT.Satria.</a>
         <a href="#" class="burger" data-bs-toggle="collapse" data-bs-target="#main-navbar">
             <span></span>
         </a>
     </div>
 </nav>
