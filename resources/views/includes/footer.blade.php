<footer class="footer" role="contentinfo">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <p class="mb-1">&copy; Copyright AqrisT.Satria. All Rights Reserved</p>
                <div class="credits">
                    Designed by <a href="https://aqristsatria.my.id/">AqrisT.Satria</a>
                </div>
            </div>
        </div>
    </div>
</footer>

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i
        class="bi bi-arrow-up-short"></i></a>
