<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index()
    {
        $projects = Project::all();

        return view('welcome.index', compact('projects'));
    }

    public function showBySlug($slug)
    {
        $project = Project::where('slug', $slug)->firstOrFail();

        return view('welcome.show', compact('project'));
    }

    public function about()
    {
        return view('welcome.about');
    }
}
