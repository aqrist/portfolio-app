<?php

namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProjectController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $projects = Project::all();

        return view('pages.projects.index', compact('projects'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pages.projects.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'category' => 'required|string',
            'portfolio' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'description' => 'required|string',
            'url' => 'nullable',
        ]);

        // Handle image upload
        $imagePath = $request->file('portfolio')->store('public/project_images');

        Project::create([
            'name' => $request->input('name'),
            'category' => $request->input('category'),
            'portfolio' => $imagePath,
            'description' => $request->input('description'),
            'url' => $request->input('url'),
        ]);

        return redirect()->route('projects.index')
            ->with('success', 'Project created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $item = Project::findOrFail($id);

        return view('pages.projects.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $project = Project::findOrFail($id);

        return view('pages.projects.edit', compact('project'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'category' => 'required|string',
            'description' => 'required|string',
            'url' => 'nullable',
        ]);

        $project = Project::findOrFail($id);

        // Update project details
        $project->update([
            'name' => $request->input('name'),
            'category' => $request->input('category'),
            'description' => $request->input('description'),
            'url' => $request->input('url'),
        ]);

        // If a new image is uploaded, update the image
        if ($request->hasFile('portfolio')) {
            $request->validate([
                'portfolio' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);

            // Handle image upload
            $imagePath = $request->file('portfolio')->store('public/project_images');

            // Update the project's portfolio image path
            $project->update(['portfolio' => $imagePath]);
        }

        return redirect()->route('projects.index')->with('success', 'Project updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        // Delete the image file when deleting the project

        $project = Project::findOrFail($id);

        if ($project->portfolio) {
            Storage::delete($project->portfolio);
        }

        $project->delete();

        return redirect()->route('projects.index')
            ->with('success', 'Project deleted successfully');
    }
}
